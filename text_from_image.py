
# TODO - Function that accepts image as an argument and returns the text in that image as a list

import pytesseract
pytesseract.pytesseract.tesseract_cmd = r"C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe"
from PIL import Image


# class TextExtraction():

# 	def __init__(self, imagePath):
# 		self.imagePath = imagePath


# 	def ocr_extract(self):

# 		im = Image.open(self.imagePath)
# 		extracted_text = pytesseract.image_to_string(im, lang = "eng")

# 		return extracted_text

class TextExtraction():

	def __init__(self):
		#self.imagePath = imagePath
		pass

	def ocr_extract(self, imagePath):

		im = Image.open(imagePath)
		extracted_text = pytesseract.image_to_string(im, lang = "eng")

		return extracted_text

if __name__ == "__main__":
	tess_obj = TextExtraction()
	print(tess_obj.ocr_extract("resources/sample_image.jpg"))

