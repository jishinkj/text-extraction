import PyPDF2

from time import time

# This function will extract and return the pdf file text content.
def extractPdfText(filePath=''):
    #=============================================================================
    # MY CODE
    #=============================================================================

    # Open the pdf file in read binary mode.
    fileObject = open(filePath, 'rb')

    # Create a pdf reader .
    pdfFileReader = PyPDF2.PdfFileReader(fileObject)

    # Get total pdf page number.
    totalPages = pdfFileReader.numPages

    # Print pdf total page number.
    print('This pdf file contains totally ' + str(totalPages) + ' pages.')
 
    textContent = list()
    for currentPage in range(totalPages):
    	pdfPage = pdfFileReader.getPage(currentPage)
    	textContent.append(pdfPage.extractText())

    return textContent

    #=============================================================================
    #=============================================================================

if __name__ == '__main__': 

    for i in range(10):

        pdfFileName = input("Enter the name of the PDF file - ")
        print("==============================================================")
        pdfFilePath = 'resources/' + str(pdfFileName) + ".pdf"
        #  print(pdfFilePath)
        start = time()
        if pdfFileName == 'c':
            break
        else:
            pdfText = extractPdfText(pdfFilePath)
            print('There are ' + str(pdfText.__len__()) + ' words in the pdf file.')
            print("\n", len(pdfText))
            end = time()
            total_time = end - start
            print("==============================================================")
            print("Total execution time is " + str(total_time) + " seconds")
            print("==============================================================")
            print("To exit, type 'c' and hit Enter")
            print("\n")