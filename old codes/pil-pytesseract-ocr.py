import pytesseract
pytesseract.pytesseract.tesseract_cmd = r"C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe"
from PIL import Image


im = Image.open("sample_image.jpg")

text = pytesseract.image_to_string(im, lang = "eng")

print(text)

#==============END OF ORIGINAL CODE================================


